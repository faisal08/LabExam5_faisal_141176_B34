<?php
$startDate = new DateTime("1981-11-03 3:20:30");
$endDate = new DateTime( date("Y-m-d h:i:sa") );

$difference = $startDate->diff($endDate);
echo "Difference : " . $difference->y . " years, " . $difference->m." months, ".$difference->d." days ".$difference->s."\n";
?>