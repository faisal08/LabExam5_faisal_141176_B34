<?php
class Factorial_of_a_number
{
    private $number;
    private $fact=1;

    public function __construct($num)
    {
        if (!is_int($num)) {
            throw new InvalidArgumentException('Not a number or missing argument');
        }
        $this->number=$num;
    }
    public function factorial(){
        for($i=$this->number;$i>=1;$i--){
            $this->fact=$this->fact*$i;
        }
        echo "the factorial of ".$this->number. " is ".$this->fact;
    }
}
$obj=new Factorial_of_a_number(5);
$obj->factorial();