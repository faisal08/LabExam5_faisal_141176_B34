<?php
class Array_sort{
    private $_assort;
    private $sorted;
    public function __construct(array $assort)
    {
        $this->_assort=$assort;
    }
    public function mysort(){
        $this->sorted=$this->_assort;
        sort($this->sorted);
        return $this->sorted;
    }
}
$obj=new Array_sort(array(11, -2, 4, 35, 0, 8, -9));
print_r($obj->mysort());