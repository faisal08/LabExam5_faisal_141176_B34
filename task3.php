<?php
class Calculator{
    private $number1;
    private $number2;
    private $result;
    public function __construct($num1,$num2)
    {
        $this->number1=$num1;
        $this->number2=$num2;
    }
    public function addition(){
        $this->result=$this->number1+$this->number2;
        echo "the sum of ".$this->number1." and ".$this->number2." is: ".$this->result."<br>";
    }
    public function subtraction(){
        $this->result=$this->number1-$this->number2;
        echo "the subtraction of ".$this->number1." and ".$this->number2." is :".$this->result."<br>";
    }
    public function multiplication(){
        $this->result=$this->number1*$this->number2;
        echo "the multiplication of ".$this->number1." and ".$this->number2." is : ".$this->result."<br>";
    }
    public function division(){
        $this->result=$this->number1/$this->number2;
        echo "the division of ".$this->number1." and ".$this->number2." is: ".$this->result."<br>";
    }

}
$obj=new Calculator(10,20);
$obj->addition();
$obj->subtraction();
$obj->multiplication();
$obj->division();